package basic;

public class Task3 {
	public static void main() {
		System.out.println(sumDigits(717) == 15);
		System.out.println(sumDigits(115) == 7);
		System.out.println(sumDigits(918) == 18);
		System.out.println(sumDigits(5) == 5);
		System.out.println(sumDigits(52) == 7);
		System.out.println(sumDigits(0) == 0);
		System.out.println(sumDigits(103) == 4);
		System.out.println(sumDigits(188) == 17);
		System.out.println("Finish");
	}

	public static void main(String[] args) {
		main();
	}

	static int sumDigits(int x) {
		assert (100 <= x && x < 1000) : String.format("3-digit positive number expected but x=%d found", x);
		int result = 0;
		for (; x > 0; x /= 10)
			result += x % 10;
		return result;
	}
}
