package basic;

public class Task6 {
	public static int SECONDS_PER_MINUTE = Task4.SECONDS_PER_MINUTE;
	public static int MINUTES_PER_HOUR = Task4.MINUTES_PER_HOUR;
	public static int HOURS_PER_DAY = Task4.HOURS_PER_DAY;

	public static void main() {
		System.out.println(getTime(20423).equals("05:40:23"));
		System.out.println(getTime( 5320).equals("01:28:40"));
		System.out.println(getTime(23630).equals("06:33:50"));
		System.out.println(getTime(20381).equals("05:39:41"));
		System.out.println(getTime(42718).equals("11:51:58"));
		System.out.println(getTime(30002).equals("08:20:02"));
		System.out.println(getTime( 3010).equals("00:50:10"));
		System.out.println(getTime(20437).equals("05:40:37"));
		System.out.println(getTime(40315).equals("11:11:55"));
		System.out.println(getTime(18392).equals("05:06:32"));
		System.out.println("Finish");
	}

	public static void main(String[] args) {
		main();
	}

	static String getTime(int secs) {
		assert (secs >= 0) : String.format("Positive time interval expected but negative secs=%d found", secs);
		return String.format("%02d:%02d:%02d", getHours(secs), getMinutes(secs), getSeconds(secs));
	}

	static int getHours(int seconds) {
		return seconds / (SECONDS_PER_MINUTE * MINUTES_PER_HOUR) % HOURS_PER_DAY;
	}

	static int getMinutes(int seconds) {
		return seconds / SECONDS_PER_MINUTE % MINUTES_PER_HOUR;
	}

	static int getSeconds(int seconds) {
		return seconds % SECONDS_PER_MINUTE;
	}
}
