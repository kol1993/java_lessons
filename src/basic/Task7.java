package basic;

public class Task7 {
	public static void main() {
		System.out.println(getNumSquares(17, 16, 1) == 272);
		System.out.println(getFreeSpace(17, 16, 1) == 0);
		System.out.println(getNumSquares(20, 10, 4) == 10);
		System.out.println(getFreeSpace(20, 10, 4) == 40);
		System.out.println(getNumSquares(14, 19, 4) == 12);
		System.out.println(getFreeSpace(14, 19, 4) == 74);
		System.out.println(getNumSquares(14, 15, 6) == 4);
		System.out.println(getFreeSpace(14, 15, 6) == 66);
		System.out.println(getNumSquares(17, 18, 2) == 72);
		System.out.println(getFreeSpace(17, 18, 2) == 18);
		System.out.println(getNumSquares(95, 59, 40) == 2);
		System.out.println(getFreeSpace(95, 59, 40) == 2405);
		System.out.println(getNumSquares(66, 73, 22) == 9);
		System.out.println(getFreeSpace(66, 73, 22) == 462);
		System.out.println(getNumSquares(55, 70, 10) == 35);
		System.out.println(getFreeSpace(55, 70, 10) == 350);
		System.out.println(getNumSquares(57, 61, 34) == 1);
		System.out.println(getFreeSpace(57, 61, 34) == 2321);
		System.out.println(getNumSquares(61, 85, 27) == 6);
		System.out.println(getFreeSpace(61, 85, 27) == 811);
		System.out.println("Finish");
	}

	public static void main(String[] args) {
		main();
	}

	static int getNumSquares(int a, int b, int c) {
		assert (a > 0): String.format("Positive integer expected as a rectangle side but a=%d found", a);
		assert (b > 0): String.format("Positive integer expected as a rectangle side but b=%d found", b);
		assert (c > 0): String.format("Positive integer expected as a square side but a=%d found", c);
		a /= c;
		b /= c;
		return a*b;
	}

	static int getFreeSpace(int a, int b, int c) {
		assert (a > 0): String.format("Positive integer expected as a rectangle side but a=%d found", a);
		assert (b > 0): String.format("Positive integer expected as a rectangle side but b=%d found", b);
		assert (c > 0): String.format("Positive integer expected as a square side but a=%d found", c);
		return a*b - c*c*getNumSquares(a, b, c);
	}
}
