package basic;
public class Task1 {
	public static void main() {
		System.out.println(canBeTriangle(3, 4, 5));
		System.out.println(canBeTriangle(0.1, 0.1, 0.1));
		System.out.println(canBeTriangle(11.4, 0.8, 11));
		System.out.println(canBeTriangle(15, 30, 15));
		System.out.println(canBeTriangle(0, 4, 4));
		System.out.println("Finish");
	}

	public static void main(String[] args) {
		main();
	}

	static double max(double a, double b, double c) {
		return Math.max(Math.max(a, b), c);
	}

	static boolean canBeTriangle(double a, double b, double c) {
		double max_abc = max(a, b, c);
		return a > 0 && b > 0 && c > 0 && a + b + c - max_abc > max_abc;
	}
}
