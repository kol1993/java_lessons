package basic;

import java.util.Set;
import java.util.TreeSet;

public class Task8 {
	public static void main() {
		System.out.println(allDigitsUnique(101)==false);
		System.out.println(allDigitsUnique(566)==false);
		System.out.println(allDigitsUnique(646)==false);
		System.out.println(allDigitsUnique(273)==true);
		System.out.println(allDigitsUnique(859)==true);
		System.out.println(allDigitsUnique(264)==true);
		System.out.println(allDigitsUnique(794)==true);
		System.out.println(allDigitsUnique(660)==false);
		System.out.println(allDigitsUnique(288)==false);
		System.out.println(allDigitsUnique(273)==true);
		System.out.println(allDigitsUnique(252)==false);
		System.out.println(allDigitsUnique(633)==false);
		System.out.println(allDigitsUnique(706)==true);
		System.out.println(allDigitsUnique(776)==false);
		System.out.println(allDigitsUnique(772)==false);
		System.out.println(allDigitsUnique(262)==false);
		System.out.println(allDigitsUnique(461)==true);
		System.out.println(allDigitsUnique(962)==true);
		System.out.println(allDigitsUnique(896)==true);
		System.out.println(allDigitsUnique(343)==false);
		System.out.println("Finish");
	}

	public static void main(String[] args) {
		main();
	}

	static boolean allDigitsUnique(int x) {
		assert (100 <= x && x < 1000) : String.format("3-digit positive number expected but x=%d found", x);
		Set<Integer> digits_set = new TreeSet<>();
		int digit = 0;
		for (; x > 0; x /= 10) {
			digit = x % 10;
			if (digits_set.contains(digit))
				return false;
			digits_set.add(digit);
		}
		return true;
	}
}
