package basic;

public class Task2 {
	public static void main() {
		System.out.println(reversNumber(717) == 717);
		System.out.println(reversNumber(411) == 114);
		System.out.println(reversNumber(517) == 715);
		System.out.println(reversNumber(240) == 42);
		System.out.println(reversNumber(900) == 9);
		System.out.println(reversNumber(333) == 333);
		System.out.println("Finish");
	}

	public static void main(String[] args) {
		main();
	}

	static int reversNumber(int x) {
		assert (100 <= x && x < 1000) : String.format("3-digit positive number expected but x=%d found", x);
		int result = 0;
		for (; x > 0; x /= 10)
			result = result * 10 + x % 10;
		return result;
	}
}
