package arrays;

public class Task8 {
    public static void main(String args[]) {
		System.out.println(calcNumRoutes(3)==1);

		System.out.println(calcNumRoutes(4)==1);

		System.out.println(calcNumRoutes(5)==3);

		System.out.println(calcNumRoutes(1)==0);

		System.out.println(calcNumRoutes(10)==14);

		System.out.println(calcNumRoutes(22)==912);

		System.out.println(calcNumRoutes(36)==134283);

		System.out.println(calcNumRoutes(40)==560287);

		System.out.println(calcNumRoutes(50)==19908673);

		System.out.println(calcNumRoutes(61)==1011015164);


        System.out.println("Finish");
    }

    static int calcNumRoutes(int n) {
		assert (n >= 2) : String.format("Integer greater or equal to 2 expected but n=%d found", n);
		int way_size = n + 1;
		int[] routes = new int[way_size];
		routes[0] = 1; // point 0 is start of the way so there is only one way to reach point 0

		for (int i = 1; i < way_size; ++i)
			routes[i] = 0; // and we know nothing about other points

		// let's start thinking
		for (int i = 0; i < way_size; ++i) {
			if (i + 2 < way_size)
				routes[i + 2] += routes[i]; // there are routes[i] more ways to reach point i+2
			if (i + 3 < way_size)
				routes[i + 3] += routes[i]; // there are routes[i] more ways to reach point i+3
			if (i + 5 < way_size)
				routes[i + 5] += routes[i]; // there are routes[i] more ways to reach point i+5
		}
		return routes[way_size - 1];
    }
}
